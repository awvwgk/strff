strff
=====

[![pipeline status](https://gitlab.com/everythingfunctional/strff/badges/master/pipeline.svg)](https://gitlab.com/everythingfunctional/strff/commits/master)

A library of string functions for Fortran.
